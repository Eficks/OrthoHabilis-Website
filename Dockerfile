# Build image
FROM node:8-alpine as builder

RUN apk add --no-cache bash git \
	&& yarn global add vue-cli

WORKDIR /app

COPY package.json ./
COPY package-lock.json /app/
RUN npm install

COPY . ./

RUN npm run build

# Target image
FROM node:8-alpine
WORKDIR /app
EXPOSE 3000

COPY --from=builder /app/.nuxt /app/.nuxt
COPY --from=builder /app/package.json /app/package.json
COPY --from=builder /app/node_modules /app/node_modules
COPY --from=builder /app/static /app/static

CMD [ "npm", "run", "start" ]
