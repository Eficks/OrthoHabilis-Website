import Axios from "axios";

export const state = () => ({
  isLogged: false,
  userId: 0,
  level: 0,
  firstname: '',
  lastname: ''
})

export const mutations = {
  login(state, { userId, level, firstname, lastname }: { userId: number, level: number, firstname: string, lastname: string }) {
    state.isLogged = true;
    state.userId = userId;
    state.firstname = firstname;
    state.lastname = lastname;
    state.level = level;
  },

  logout(state) {
    state.isLogged = false;
  },
}


export const actions = {
  // async nuxtServerInit({ commit }, { req }) {
  //   const auth = await Axios.post("http://server:8080/services/auth", {
  //     method: "get",
  //     params: [{}],
  //     id: 0,
  //     jsonrpc: "2.0"
  //   }, req?{ headers: req.headers }:{});
  //   if (!auth.data.error) {
  //     commit("login", { userId: auth.data.result.id, firstname: auth.data.result.firstname, lastname: auth.data.result.lastname });
  //   } else {
  //     commit("logout");
  //   }
  // },

  async get ({ commit }) {
    console.log('get');
    const auth = await Axios.post("/services/auth", {
      method: "get",
      params: [{}],
      id: 0,
      jsonrpc: "2.0"
    });
    if (!auth.data.error) {
      commit("login", { userId: auth.data.result.id, firstname: auth.data.result.firstname, lastname: auth.data.result.lastname });
    } else {
      commit("logout");
    }
  },

  async login({ commit }) {
    const auth = await Axios.post("/services/auth", {
      method: "regularLogin",
      params: [{ email: "fx.denys@outlook.be", password: "bidons" }],
      id: 6,
      jsonrpc: "2.0"
    });
    commit("login", { userId: auth.data.result.id, firstname: auth.data.result.firstname, lastname: auth.data.result.lastname });
  },

  async logout({ commit }) {
    const auth = await Axios.post("/services/auth", {
      method: "logout",
      params: [{}],
      id: 6,
      jsonrpc: "2.0"
    });
    commit("logout");
  }
}
