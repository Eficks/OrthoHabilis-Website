const PurgecssPlugin = require('purgecss-webpack-plugin')
const glob = require('glob-all')
const path = require('path')

const parseArgs = require("minimist")
const argv = parseArgs(process.argv.slice(2), {
  alias: {
    H: "hostname",
    p: "port"
  },
  string: ["H"],
  unknown: parameter => false
})

const port =
  argv.port ||
  process.env.PORT ||
  process.env.npm_package_config_nuxt_port ||
  "3000"
const host =
  argv.hostname ||
  process.env.HOST ||
  process.env.npm_package_config_nuxt_host ||
  "localhost"
module.exports = {
  env: {
    baseUrl:
      process.env.BASE_URL ||
      `http://${host}:${port}`
  },
  head: {
    title: "tt1",
    meta: [
      { charset: "utf-8" },
      {
        name: "viewport",
        content:
          "width=device-width, initial-scale=1"
      },
      {
        hid: "description",
        name: "description",
        content: "Nuxt.js project"
      }
    ],
    script: [
      { src: '/js/jquery-2.2.4.min.js', defer:'' },
      { src: '/rs-plugin/js/jquery.themepunch.plugins.min.js', defer:''  },
      { src: '/rs-plugin/js/jquery.themepunch.revolution.min.js', defer:'' },
  
      { src: '/js/superfish.js', defer:'' },

      // { src: '/js/bootstrap.min.js', defer:'' },
      // { src: '/js/retina.min.js' },
      // { src: '/js/validate.js' },
      // { src: '/js/jquery.placeholder.js' },
      // { src: '/js/functions.js' },
      // { src: '/js/classie.js' },
      // { src: '/js/uisearch.js' }
        
    ],    
    link: [
      {
        rel: "icon",
        type: "image/x-icon",
        href: "/favicon.ico"
      }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: "#3B8070" },
  /*
  ** Build configuration
  */
  css: [
    '~/assets/purged-css/settings.css',
    '~/assets/purged-css/bootstrap.min.css',
    '~/assets/purged-css/superfish.css',
    '~/assets/purged-css/style.css',
    '~/assets/purged-css/fontello.css',    
  ],
  build: {
    analyze: false,
    vendor: [
      'jquery',
      'lazysizes',
    ],
    extractCSS: false,
    /*
    ** Run ESLint on save
    */
    // extend (config, { isDev, isClient }) {
    //   if (isDev && isClient) {
    //     // config.module.rules.push({
    //     //   enforce: 'pre',
    //     //   test: /\.(js|vue)$/,
    //     //   loader: 'eslint-loader',
    //     //   exclude: /(node_modules)/
    //     // })
    //   }
    //   if (!isDev) {
    //     // Remove unused CSS using purgecss. See https://github.com/FullHuman/purgecss
    //     // for more information about purgecss.
    //     config.plugins.push(
    //       new PurgecssPlugin({
    //         paths: glob.sync([
    //           path.join(__dirname, './pages/**/*.vue'),
    //           path.join(__dirname, './layouts/**/*.vue'),
    //           path.join(__dirname, './components/**/*.vue'),
    //           path.join(__dirname, './static/**/*.js')
    //         ]),
    //         whitelist: ['html', 'body']
    //       })
    //     )
    //   }
    // }
    
  },
  modules: [
    "@nuxtjs/axios",
    "~/modules/typescript.js"
  ],
  axios: {}
}
